from datetime import datetime
import pytz

from werkzeug.exceptions import BadRequest

from seta_flask_server.repository.models import StoredSearchItem, SearchItemType
from seta_flask_server.repository.interfaces import IStoredSearchesBroker


def parse_args_new_items(user_id: str, data: dict) -> StoredSearchItem:
    """Build library item models from json."""

    now = datetime.now(tz=pytz.utc)

    if not isinstance(data, dict):
        raise BadRequest("Request payload is not a dictionary.")

    item_id = StoredSearchItem.generate_uuid()

    name = data.get("name", None)
    if not name:
        raise BadRequest("Name is required.")

    item_type = data.get("type", None)
    if item_type is None:
        raise BadRequest("Type is required.")

    search = None
    if item_type == 1:
        search = data.get("search", None)
        if search is None:
            raise BadRequest("Search is required when type=1.")

    return StoredSearchItem(
        user_id=user_id,
        id=item_id,
        name=name,
        parent_id=data.get("parentId", None),
        type=item_type,
        search=search,
        created_at=now,
    )


def parse_args_update_item(item: StoredSearchItem, args: dict):
    """Parse json into search item properties."""

    if not isinstance(args, dict):
        raise BadRequest("Data is not a dictionary.")

    item.parent_id = args.get("parentId", None)
    item.name = args.get("name", None)
    item.search = args.get("search", None)
    item.modified_at = datetime.now(tz=pytz.utc)


def get_tree(user_id: str, broker: IStoredSearchesBroker) -> dict:
    """Build library tree from database entries."""

    items = {"items": []}
    items["items"] = _build_tree(user_id=user_id, broker=broker)

    return items


def _build_tree(
    user_id: str, broker: IStoredSearchesBroker, parent: dict = None
) -> list[dict]:
    """Build library tree recursively."""

    parent_id = None

    if parent is not None:
        parent_id = parent["id"]

    db_items = broker.get_all_by_parent(user_id=user_id, parent_id=parent_id)

    search_items = []

    for item in db_items:
        js_item = item.to_json_api()

        # construct the path for this item

        if parent is None:
            item_path = [item.name]
        else:
            item_path = parent["path"].copy()
            item_path.append(item.name)

        js_item["path"] = item_path

        search_items.append(js_item)

        if item.type == SearchItemType.Folder:
            js_item["children"] = _build_tree(
                user_id=user_id, broker=broker, parent=js_item
            )

    return search_items
