from http import HTTPStatus
from injector import inject

from flask import current_app
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restx import Namespace, Resource, abort

from seta_flask_server.repository import interfaces
from seta_flask_server.infrastructure.auth_helpers import (
    create_rag_token,
    compute_expire_date,
)
from .models import token_dto as dto

rag_token_ns = Namespace(
    "RAG Access Token", description="Generates access token for RAG api."
)
rag_token_ns.models.update(dto.ns_models)


@rag_token_ns.route("", endpoint="rag_token", methods=["POST"])
class RAGTokenResource(Resource):

    @inject
    def __init__(
        self, users_broker: interfaces.IUsersBroker, *args, api=None, **kwargs
    ):
        super().__init__(api, *args, **kwargs)

        self.users_broker = users_broker

    @rag_token_ns.doc(
        description="Generates access token for RAG api with an expiration date of  __maximum 180 days__ in the future.",
        responses={
            int(HTTPStatus.OK): "Public key saved.",
            int(
                HTTPStatus.BAD_REQUEST
            ): "Expires date is mising, invalid format or out of range.",
        },
        security="CSRF",
    )
    @rag_token_ns.response(int(HTTPStatus.OK), "", dto.access_token)
    @rag_token_ns.response(
        int(HTTPStatus.BAD_REQUEST), "", dto.response_dto.error_model
    )
    @rag_token_ns.expect(dto.expires_at, validate=True)
    @jwt_required(fresh=True)
    def post(self):
        """Generates access token for the user."""

        identity = get_jwt_identity()
        user_id = identity["user_id"]

        user = self.users_broker.get_user_by_id(user_id, load_scopes=False)
        if user is None or user.is_not_active():
            abort(HTTPStatus.FORBIDDEN, "Insufficient rights.")

        try:
            expires_str = rag_token_ns.payload["expires"]
            expires = compute_expire_date(expires_str)

            secret = current_app.config["RAG_SECRET_KEY"]

            access_token = create_rag_token(user, secret, expires)

            return {"accessToken": access_token}

        except ValueError as ve:
            abort(HTTPStatus.BAD_REQUEST, str(ve))

        return None
