from flask import jsonify, abort, Blueprint
from flask_restx import Api

from .token_info import ns_authorization
from .data_source_permissions import data_sources_ns

token_info_bp = Blueprint("token_info", __name__)
authorization_api = Api(
    token_info_bp,
    version="1.0",
    title="JWT Token Authorization",
    doc="/doc",
    description="JWT authorization for seta apis",
)

authorization_api.add_namespace(ns_authorization, path="/token_info")
authorization_api.add_namespace(data_sources_ns, path="/data-sources")
