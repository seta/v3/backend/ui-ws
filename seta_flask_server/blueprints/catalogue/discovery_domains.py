from http import HTTPStatus
from injector import inject

from flask_jwt_extended import jwt_required
from flask_restx import Namespace, Resource

from seta_flask_server.repository.interfaces import ICatalogueBroker
from .models import discovery_domains_dto as dto


discovery_domains_catalogue_ns = Namespace(
    "Discovery Domains", validate=False, description="List of discovery domains"
)
discovery_domains_catalogue_ns.models.update(dto.ns_models)


@discovery_domains_catalogue_ns.route("", endpoint="discovery_domains", methods=["GET"])
class DiscoveryDomainCatalogue(Resource):
    @inject
    def __init__(self, catalogue_broker: ICatalogueBroker, *args, api=None, **kwargs):
        self.catalogue_broker = catalogue_broker

        super().__init__(api, *args, **kwargs)

    @discovery_domains_catalogue_ns.doc(
        description="Retrieve fields catalogue.",
        responses={int(HTTPStatus.OK): "'Retrieved fields."},
        security="CSRF",
    )
    @discovery_domains_catalogue_ns.marshal_list_with(
        dto.discovery_domains_model, mask="*", skip_none=True
    )
    @jwt_required()
    def get(self):
        """
        Get catalogue of discovery domains
        """

        return {"domains": self.catalogue_broker.get_discovery_domains()}
