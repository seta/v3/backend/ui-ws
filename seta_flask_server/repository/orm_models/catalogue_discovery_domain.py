from seta_flask_server.infrastructure.extensions import db


class CatalogueDiscoveryDomainOrm(db.Model):
    __tablename__ = "catalogue_discovery_domains"

    code = db.Column(db.String(100), primary_key=True)
    description = db.Column(db.String(5000), nullable=True)

    def __repr__(self) -> str:
        return f"<CatalogueDiscoveryDomainOrm {self.code}>"
