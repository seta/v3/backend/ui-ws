from interface import Interface
from seta_flask_server.repository.models import StoredSearchItem


class IStoredSearchesBroker(Interface):
    def create(self, item: StoredSearchItem):
        """Inserts a new search item in the database."""

        pass

    def get_all_by_parent(self, user_id: str, parent_id: str) -> list[StoredSearchItem]:
        """Sorted children of the specified parent.

        Args:
            user_id:  Identifier of the owner.
            parent_id: Parent identifier of the returned items

        Returns:
            List of search items sorted by 'type' and 'name'
        """

        pass

    def get_by_id(self, user_id: str, item_id: str) -> StoredSearchItem:
        """Search item by internal identifier

        :param user_id:
            Identifier of the owner

        :param id:
            Item identifier
        """

        pass

    def update(self, item: StoredSearchItem):
        """Updates an item."""

        pass

    def delete(self, user_id: str, item_id: str):
        """Cascade delete a search item (all children included)."""

        pass
