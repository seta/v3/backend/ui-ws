# pylint: disable=missing-function-docstring, invalid-name
from enum import IntEnum
from dataclasses import dataclass, asdict
from datetime import datetime
import shortuuid


class SearchItemType(IntEnum):
    Folder = 0
    Document = 1


@dataclass
class StoredSearchItem:
    user_id: str
    id: str
    name: str
    parent_id: str
    type: SearchItemType
    search: dict
    created_at: datetime = None
    modified_at: datetime = None

    def to_json(self) -> dict:
        json = asdict(self)

        if self.type == SearchItemType.Folder:
            if "search" in json:
                del json["search"]

        return json

    def to_json_api(self) -> dict:
        json = {
            "id": self.id,
            "name": self.name,
            "parentId": self.parent_id,
            "type": self.type,
        }

        if self.type == SearchItemType.Document:
            json["search"] = self.search

        return json

    @classmethod
    def from_db_json(cls, json_dict: dict):
        return cls(
            user_id=json_dict["user_id"],
            id=json_dict["id"],
            name=json_dict["name"],
            parent_id=json_dict["parent_id"],
            type=json_dict["type"],
            search=json_dict.get("search", None),
            created_at=json_dict.get("created_at", None),
            modified_at=json_dict.get("modified_at", None),
        )

    @staticmethod
    def generate_uuid() -> str:
        return shortuuid.ShortUUID().random(length=12)
