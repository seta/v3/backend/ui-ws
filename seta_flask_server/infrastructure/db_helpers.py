import threading

from flask import current_app as app
from migrations.utils import current_revision, run_upgrade

DB_UPGRADED = False


def migrate_database():
    """Migrate the database."""
    global DB_UPGRADED  # pylint: disable=global-statement

    lock = threading.Lock()
    with lock:

        if DB_UPGRADED:
            app.logger.info("Database already upgraded.")
            return

        DB_UPGRADED = True

        current = current_revision()
        app.logger.info("Current database revision: %s", current)

        if current is None or current == "":
            run_upgrade()
            app.logger.info("Database initialized.")
        elif "head" not in current:

            app.logger.info("Database not at the latest version.")

            if app.config.get("DB_AUTO_UPGRADE", False):
                run_upgrade()
                app.logger.info("Database migrated - env DB_AUTO_UPGRADE is True.")
