from flask import json
from flask.testing import FlaskClient
from tests.infrastructure.helpers.util import auth_headers

API_V1 = "/seta-ui/api/v1/me/searches"


def create_item(client: FlaskClient, access_token: str, payload: dict):
    """Create items."""

    return client.post(
        API_V1,
        data=json.dumps(payload),
        content_type="application/json",
        headers=auth_headers(access_token),
    )


def get_tree(client: FlaskClient, access_token: str):
    """Get searches tree."""

    return client.get(
        API_V1,
        content_type="application/json",
        headers=auth_headers(access_token),
    )


def update_item(client: FlaskClient, access_token: str, item_id: str, payload: dict):
    """Update search item."""

    return client.put(
        f"{API_V1}/{item_id}",
        data=json.dumps(payload),
        content_type="application/json",
        headers=auth_headers(access_token),
    )


def delete_item(client: FlaskClient, access_token: str, item_id: str):
    """Delete search item."""

    return client.delete(
        f"{API_V1}/{item_id}",
        content_type="application/json",
        headers=auth_headers(access_token),
    )
