from http import HTTPStatus
import pytest
from flask.testing import FlaskClient

from tests.infrastructure.helpers.authentication import login_user
from tests.infrastructure.helpers.util import get_access_token
from tests.functional.profile.searches import api


@pytest.mark.parametrize("user_id", [("seta_admin")])
def test_create_items(
    client: FlaskClient, authentication_url: str, user_key_pairs: dict, user_id: str
):
    """Test creating searches items"""

    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    folder_payload = {"parentId": None, "name": "Test", "type": 0}
    response = api.create_item(
        client=client, access_token=access_token, payload=folder_payload
    )

    assert response.status_code == HTTPStatus.CREATED
    assert response.json["name"] == "Test"
    assert "id" in response.json

    item_id = response.json["id"]

    # Prepare the payload for creating searches items
    search_items = [
        {
            "parentId": item_id,
            "name": "My first search",
            "search": {"query": "covid", "filters": {"source": "cordis"}},
            "type": 1,
        },
        {
            "parentId": item_id,
            "name": "My second search",
            "search": {"query": "vaccine", "filters": {"source": "cellar"}},
            "type": 1,
        },
    ]

    for item in search_items:
        response = api.create_item(
            client=client, access_token=access_token, payload=item
        )
        assert response.status_code == HTTPStatus.CREATED


@pytest.mark.parametrize("user_id", [("seta_admin")])
def test_get_tree(
    client: FlaskClient, authentication_url: str, user_key_pairs: dict, user_id: str
):
    """Test getting search tree."""

    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    response = api.get_tree(client=client, access_token=access_token)
    assert response.status_code == HTTPStatus.OK
    assert "items" in response.json


@pytest.mark.parametrize("user_id", [("seta_admin")])
def test_update_item(
    client: FlaskClient, authentication_url: str, user_key_pairs: dict, user_id: str
):
    """Test updating search item."""

    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    payload = {
        "parentId": None,
        "name": "Test update",
        "search": {"query": "commission", "filters": {"source": "cordis"}},
    }

    response = api.get_tree(client=client, access_token=access_token)
    assert len(response.json["items"]) > 0
    assert "children" in response.json["items"][0]
    assert len(response.json["items"][0]["children"]) > 0
    item_id = response.json["items"][0]["children"][0]["id"]

    response = api.update_item(
        client=client, access_token=access_token, item_id=item_id, payload=payload
    )
    assert response.status_code == HTTPStatus.OK

    response = api.get_tree(client=client, access_token=access_token)
    assert len(response.json["items"]) > 0
    assert "children" in response.json["items"][0]

    for item in response.json["items"][0]["children"]:
        if item["id"] == item_id:
            assert item["name"] == "Test update"
            assert "query" in item["search"] and item["search"]["query"] == "commission"
            break


@pytest.mark.parametrize("user_id", [("seta_admin")])
def test_delete_item(
    client: FlaskClient, authentication_url: str, user_key_pairs: dict, user_id: str
):
    """Test delete search item."""

    response = login_user(
        auth_url=authentication_url, user_key_pairs=user_key_pairs, user_id=user_id
    )
    access_token = get_access_token(response)

    response = api.get_tree(client=client, access_token=access_token)
    assert len(response.json["items"]) > 0
    assert "children" in response.json["items"][0]

    children_len = len(response.json["items"][0]["children"])
    assert children_len > 0
    item_id = response.json["items"][0]["children"][0]["id"]

    response = api.delete_item(
        client=client, access_token=access_token, item_id=item_id
    )
    assert response.status_code == HTTPStatus.OK

    response = api.get_tree(client=client, access_token=access_token)
    assert len(response.json["items"]) > 0
    assert "children" in response.json["items"][0]
    assert len(response.json["items"][0]["children"]) < children_len
