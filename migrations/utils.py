from subprocess import STDOUT, check_output


def current_revision() -> str | None:
    """Get the current revision of the database."""

    try:
        output = check_output(
            "cd /home/seta/ && flask --app app_migrate db current",
            stderr=STDOUT,
            shell=True,
        )
    except Exception:
        return None
    return output.decode("utf-8").strip()


def run_upgrade() -> str:
    """Run the database upgrade."""

    output = check_output(
        "cd /home/seta/ && flask --app app_migrate db upgrade",
        stderr=STDOUT,
        shell=True,
    )

    return output.decode("utf-8")
