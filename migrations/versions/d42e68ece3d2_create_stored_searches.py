# pylint:disable-all

"""create stored_searches

Revision ID: d42e68ece3d2
Revises: 4a0be77d8377
Create Date: 2024-08-14 10:28:40.616270

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d42e68ece3d2"
down_revision = "4a0be77d8377"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "stored_searches",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("item_id", sa.String(length=36), unique=True, nullable=False),
        sa.Column(
            "user_id",
            sa.String(length=36),
            sa.ForeignKey("users.user_id"),
            nullable=False,
        ),
        sa.Column("parent_id", sa.String(length=36), nullable=True),
        sa.Column("item_type", sa.Integer, nullable=False),
        sa.Column("name", sa.String(5000), nullable=False),
        sa.Column("search", sa.JSON, nullable=True),
        sa.Column("created_at", sa.DateTime, nullable=False),
        sa.Column(
            "modified_at",
            sa.DateTime,
            nullable=True,
        ),
    )


def downgrade():
    op.drop_table("stored_searches")
