# pylint:disable-all

"""discovery domains

Revision ID: c43b0e067571
Revises: d42e68ece3d2
Create Date: 2024-12-04 11:13:18.042295

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "c43b0e067571"
down_revision = "d42e68ece3d2"
branch_labels = None
depends_on = None


def upgrade():

    ### Create and seed discovery_domains table

    op.create_table(
        "catalogue_discovery_domains",
        sa.Column("code", sa.String(length=100), nullable=False, primary_key=True),
        sa.Column("description", sa.String(), nullable=True),
    )

    op.bulk_insert(
        sa.Table(
            "catalogue_discovery_domains",
            sa.MetaData(),
            autoload_with=op.get_bind(),
        ),
        [
            {
                "code": "search",
                "description": "Data source is discoverable on corpus search.",
            },
            {
                "code": "rag",
                "description": "Data source is discoverable on RAG queries.",
            },
            {
                "code": "nlp",
                "description": "Data source is used for text processing applications.",
            },
        ],
    )

    #############

    ### Add discovery domains array to data_sources

    with op.batch_alter_table("data_sources", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("discovery_domains", sa.ARRAY(sa.String()), nullable=True)
        )

    op.execute(
        "UPDATE data_sources SET discovery_domains = ARRAY['search'] WHERE discovery_domains IS NULL;"
    )

    #############


def downgrade():
    with op.batch_alter_table("discovery_domains", schema=None) as batch_op:
        batch_op.drop_column("discovery_domains")

    op.drop_table("catalogue_discovery_domains")
