# pylint:disable-all

"""Delete community_id from fields catalogue

Revision ID: 9b37db846033
Revises: af4aee93da0c
Create Date: 2024-07-02 08:51:33.960507

"""

from alembic import op


# revision identifiers, used by Alembic.
revision = "9b37db846033"
down_revision = "af4aee93da0c"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("DELETE FROM catalogue_fields WHERE name = 'community_id'")


def downgrade():
    pass
