# pylint:disable-all

"""alter library

Revision ID: 4a0be77d8377
Revises: 9b37db846033
Create Date: 2024-08-14 08:34:01.585872

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4a0be77d8377"
down_revision = "9b37db846033"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        "library",
        "title",
        type_=sa.String(length=65000),
        existing_type=sa.String(length=200),
    )

    op.alter_column(
        "library",
        "link",
        type_=sa.String(length=5000),
        existing_type=sa.String(length=255),
    )

    op.alter_column(
        "library",
        "source",
        type_=sa.String(length=1000),
        existing_type=sa.String(length=300),
    )


def downgrade():
    pass
