# rest-ws

## Web API
Flask web API for SeTA web application

## Container Registry
If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username and password. 
If you have Two-Factor Authentication enabled, use a [Personal Access Token](https://code.europa.eu/help/user/profile/personal_access_tokens) instead of a password.

Login with Personal Access Token:
```
docker login code.europa.eu:4567
```

### Build UI Image 
Build production image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/backend/rest-ws/ui:{$VERSION} . -f dockerfiles/Dockerfile-ui
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/backend/rest-ws/ui:{$VERSION}
```

### Build AUTH Image 
Build production image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/backend/rest-ws/auth:{$VERSION} . -f dockerfiles/Dockerfile-auth
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/backend/rest-ws/auth:{$VERSION}
```

### Build Images for Development 

First, clone `code.europa.eu/seta/v3/composer/docker-compose` project and run `docker compose up` - check project documentation

Create the `.env` file in `./compose-dev` folder and run:

```
docker compose build
docker compose up
```

The up command recreates the existing 'auth', 'ui' and 'nginx' containers.